const express = require('express')
const bodyParser = require('body-parser')
const createProductService = require('./services/product')
const ProductRepository = require('../data/product-repository')

const app = express()
const productService = createProductService(new ProductRepository())

app.use(bodyParser.json())

app.get('/products', productService.all)
app.get('/products/:id', productService.get)
app.post('/products', productService.create)
app.patch('/products/:id', productService.update)
app.delete('/products/:id', productService.remove)

app.listen(3000, () => {
  console.info('Server listen in port 3000')
})
