const Product = require('../../core/product')
const createProduct = require('../../core/interactor/create-product')
const getProduct = require('../../core/interactor/get-product')
const getProductList = require('../../core/interactor/get-product-list')
const updateProduct = require('../../core/interactor/update-product')
const removeProduct = require('../../core/interactor/remove-product')

const createProductService = (repository) => ({
  all (req, res) {
    getProductList(repository)
      .then(products => res.json({ products }))
      .catch(err => res.status(500).json(err))
  },

  get (req, res) {
    getProduct(repository, req.params.id)
      .then(product => res.json(product))
      .catch(err => res.status(500).json(err))
  },

  // TODO: Don't use the product entity
  // TODO: Validate request data
  create (req, res) {
    const product = new Product(req.body.code, req.body.name, req.body.price)
    createProduct(repository, product)
      .then(product => res.status(201).json(product))
      .catch(err => res.status(500).json(err))
  },

  // TODO: Don't use the product entity
  // TODO: Validate request data
  update (req, res) {
    const product = new Product(req.body.code, req.body.name, req.body.price)
    updateProduct(repository, req.params.id, product)
      .then(() => res.status(204).end())
      .catch(err => res.status(500).json(err))
  },

  remove (req, res) {
    removeProduct(repository, req.params.id)
      .then(() => res.status(204).end())
      .catch(err => res.status(500).json(err))
  }
})

module.exports = createProductService
