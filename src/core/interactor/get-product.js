const getProduct = (repository, id) => repository.get(id)

module.exports = getProduct
