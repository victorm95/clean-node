const removeProduct = (repository, id) => repository.remove(id)

module.exports = removeProduct
