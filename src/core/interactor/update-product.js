const updateProduct = (repository, id, product) => repository.update(id, product)

module.exports = updateProduct
