const Datastore = require('nedb')

class ProductRepository {
  constructor () {
    this._datastore = new Datastore()
  }

  all () {
    return new Promise((resolve, reject) => {
      this._datastore.find({}, (err, products) => {
        if (err) {
          reject(err)
          return
        }
        resolve(products)
      })
    })
  }

  get (_id) {
    return new Promise((resolve, reject) => {
      this._datastore.findOne({ _id }, (err, product) => {
        if (err) {
          reject(err)
          return
        }
        resolve(product)
      })
    })
  }

  add (product) {
    return new Promise((resolve, reject) => {
      this._datastore.insert(product, (err, newProduct) => {
        if (err) {
          reject(err)
          return
        }
        resolve(newProduct)
      })
    })
  }

  update (_id, product) {
    return new Promise((resolve, reject) => {
      this._datastore.update({ _id }, product, {}, (err, docs) => {
        if (err) {
          reject(err)
          return
        }
        resolve(null)
      })
    })
  }

  remove (_id) {
    return new Promise((resolve, reject) => {
      this._datastore.remove({ _id }, {}, (err, docs) => {
        if (err) {
          reject(err)
          return
        }
        resolve(null)
      })
    })
  }
}

module.exports = ProductRepository
